﻿using System;
using Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Viewers
{
    public class EnterToGameView : MonoBehaviour
    {
        [SerializeField] private TMP_Text dataPatchText;
        [SerializeField] private Button openDataPatchButton;
        [Space]
        [SerializeField] private GameObject mainPanel;
        [SerializeField] private TMP_Dropdown gamesDropdown;
        [SerializeField] private Button startButton;
        [SerializeField] private Button newGameButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Button renameGameButton;
        [Space]
        [SerializeField] private GameObject renamePanel;
        [SerializeField] private TMP_InputField renameInputText;
        [SerializeField] private Button renameOkButton;
        [SerializeField] private Button cancelRenameButton;
        [Space]
        [SerializeField] private GameObject newGamePanel;
        [SerializeField] private TMP_InputField newGameNameInputText;
        [SerializeField] private Button newGameOkButton;
        [SerializeField] private Button cancelNewGameButton;
        [SerializeField] private TMP_Dropdown newGameTypeDropdown;

        private void Awake()
        {
            renamePanel.SetActive(false);
            newGamePanel.SetActive(false);

            EnterToGameModel.DataPatchText.AddText(dataPatchText);
            EnterToGameModel.GameSelectDropdown.AddDropdown(gamesDropdown);
            EnterToGameModel.RenameGameInputField.AddInputField(renameInputText);
            EnterToGameModel.StartGameButton.AddButton(startButton);
            EnterToGameModel.ExitButton.AddButton(exitButton);
            EnterToGameModel.RenameGameOkButton.AddButton(renameOkButton);
            EnterToGameModel.OpenDataDirectoryButton.AddButton(openDataPatchButton);
            EnterToGameModel.CreateNewGameButton.AddButton(newGameOkButton);
            EnterToGameModel.NewGameNameInputField.AddInputField(newGameNameInputText);
            EnterToGameModel.GameTypesDropdown.AddDropdown(newGameTypeDropdown);

            renameGameButton.onClick.AddListener(delegate
            {
                var selectedGame = gamesDropdown.value;
                renameInputText.text = EnterToGameModel.Games[selectedGame];
                renamePanel.SetActive(true);
                mainPanel.SetActive(false);
                CheckNewName(renameInputText.text);
            });

            renameOkButton.onClick.AddListener(delegate
            {
                renamePanel.SetActive(false);
                mainPanel.SetActive(true);
            });
            
            newGameOkButton.onClick.AddListener(delegate
            {
                newGamePanel.SetActive(false);
                mainPanel.SetActive(true);
            });

            cancelRenameButton.onClick.AddListener(delegate
            {
                renamePanel.SetActive(false);
                mainPanel.SetActive(true);
            });

            renameInputText.onValueChanged.AddListener(CheckNewName);
            newGameNameInputText.onValueChanged.AddListener(CheckNewName);
            
            newGameButton.onClick.AddListener(delegate
            {
                newGamePanel.SetActive(true);
                mainPanel.SetActive(false);
                newGameNameInputText.text = String.Empty;
                CheckNewName(newGameNameInputText.text);
            });
            
            cancelNewGameButton.onClick.AddListener(delegate
            {
                newGamePanel.SetActive(false);
                mainPanel.SetActive(true);
            });
        }

        private void CheckNewName(string newGameName)
        {
            if (string.IsNullOrEmpty(newGameName))
            {
                renameOkButton.interactable = false;
                newGameOkButton.interactable = false;
                return;
            }
            
            var goodName = true;

            for (int i = 0; i < EnterToGameModel.Games.Count; i++)
            {
                var game = EnterToGameModel.Games[i];
                var comparison = StringComparison.InvariantCultureIgnoreCase;
                if (!String.Equals(newGameName, game, comparison)) continue;
                
                goodName = false;
                break;
            }

            renameOkButton.interactable = goodName;
            newGameOkButton.interactable = goodName;
        }
    }
}