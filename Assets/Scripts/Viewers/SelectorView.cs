﻿using Models;
using Models.Database;
using UnityEngine;
using UnityEngine.UI;

namespace Viewers
{
    public class SelectorView : MonoBehaviour
    {
        [SerializeField]
        private Button mapButton;

        [SerializeField]
        private Button databaseButton;

        [SerializeField]
        private Button timeTrackerButton;

        [SerializeField]
        private Button combatButton;

        [SerializeField]
        private Button settingsButton;

        private void Awake()
        {
            Model.MapButtonController.AddButton(mapButton);
            Model.DatabaseButtonController.AddButton(databaseButton);
            Model.TimeTrackerButtonController.AddButton(timeTrackerButton);
            Model.CombatButtonController.AddButton(combatButton);
            Model.SettingsButtonController.AddButton(settingsButton);
            
            Core.OnCurrentModelChange += CurrentModelChangeHandler;
            CurrentModelChangeHandler(Core.CurrentModel);
        }

        private void CurrentModelChangeHandler(Model newModel)
        {
            mapButton.interactable = !(newModel is MapModel);
            databaseButton.interactable = !(newModel is DatabaseModel);
            timeTrackerButton.interactable = !(newModel is TimeTrackerModel);
            combatButton.interactable = !(newModel is CombatModel);
            settingsButton.interactable = !(newModel is SettingsModel);
        }
    }
}