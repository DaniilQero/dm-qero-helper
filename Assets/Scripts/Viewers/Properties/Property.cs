﻿namespace Viewers.Properties
{
    public abstract class Property<T>
    {
        public T value { get; private set; }
        
        public virtual void SetValue(T value)
        {
            this.value = value;
        }
    }
}