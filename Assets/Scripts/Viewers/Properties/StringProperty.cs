﻿using System.Collections.Generic;
using TMPro;

namespace Viewers.Properties
{
    public class StringProperty : Property<string>
    {
        private readonly List<TMP_Text> texts = new List<TMP_Text>(1);

        public void AddText(TMP_Text text)
        {
            texts.Add(text);
            text.text = value;
        }

        public override void SetValue(string newValue)
        {
            base.SetValue(newValue);
            for (int i = 0; i < texts.Count; i++)
            {
                var text = texts[i];
                if (text == null)
                {
                    texts.RemoveAt(i);
                    i--;
                    continue;
                }

                text.text = newValue;
            }
        }
    }
}