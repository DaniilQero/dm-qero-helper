﻿using System.Collections.Generic;
using TMPro;

namespace Viewers.Properties
{
    public class StringArrayProperty : Property<string[]>
    {
        private readonly Dictionary<int, List<TMP_Text>> texts = new(1);
        
        public void AddText(TMP_Text text, int id)
        {
            if (!texts.TryGetValue(id, out var list))
            {
                list = new List<TMP_Text>();
                texts.Add(id,list);
            }
            
            list.Add(text);
            text.text = value[id];
        }
        
        public override void SetValue(string[] newValue)
        {
            base.SetValue(newValue);
            for (int i = 0; i < newValue.Length; i++)
            {
                if (texts.TryGetValue(i, out var list))
                {
                    for (int j = 0; j < list.Count; j++)
                    {
                        var text = list[j];
                        if (text == null)
                        {
                            list.RemoveAt(j);
                            j--;
                            continue;
                        }

                        text.text = newValue[i];
                    }
                }
            }
        }
    }
}