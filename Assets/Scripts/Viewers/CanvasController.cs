﻿using Models;
using Models.Database;
using UnityEngine;

namespace Viewers
{
    public class CanvasController : MonoBehaviour
    {
        [SerializeField]
        private GameObject loading;

        [SerializeField]
        private GameObject enterToGame;

        [SerializeField]
        private GameObject map;

        [SerializeField]
        private GameObject database;

        [SerializeField]
        private GameObject timeTracker;

        [SerializeField]
        private GameObject combat;

        [SerializeField]
        private GameObject settings;

        [SerializeField]
        private GameObject selector;

        private void Awake()
        {
            Core.OnCurrentModelChange += CurrentModelChangeHandler;
            CurrentModelChangeHandler(Core.CurrentModel);
        }

        private void CurrentModelChangeHandler(Model newModel)
        {
            loading.SetActive(newModel is LoadingModel);
            enterToGame.SetActive(newModel is EnterToGameModel);
            map.SetActive(newModel is MapModel);
            database.SetActive(newModel is DatabaseModel);
            timeTracker.SetActive(newModel is TimeTrackerModel);
            combat.SetActive(newModel is CombatModel);
            settings.SetActive(newModel is SettingsModel);
            selector.SetActive(!(newModel is LoadingModel) && !(newModel is EnterToGameModel));
        }
    }
}