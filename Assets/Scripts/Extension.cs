﻿using Services;

namespace DefaultNamespace
{
    public static class Extension
    {
        public static string Localize(this string key)
        {
            return LocalizationManager.GetText(key);
        }
    }
}