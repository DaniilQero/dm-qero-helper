﻿using System.Collections.Generic;
using TMPro;

namespace Controllers
{
    public class InputFieldController
    {
        public string CurrentValue { get; private set; }
        
        private readonly List<TMP_InputField> inputFields = new List<TMP_InputField>(1);

        public void AddInputField(TMP_InputField inputField)
        {
            if (inputFields.Contains(inputField)) return;
            
            inputFields.Add(inputField);
            inputField.onValueChanged.AddListener(ChangeInputHandler);
        }

        public void SetText(string newValue)
        {
            ChangeInputHandler(newValue);
        }

        private void ChangeInputHandler(string newValue)
        {
            CurrentValue = newValue;
            for (int i = 0; i < inputFields.Count; i++)
            {
                var inputFiled = inputFields[i];
                if (inputFiled == null)
                {
                    inputFields.RemoveAt(i);
                    i--;
                    continue;
                }
                
                inputFiled.SetTextWithoutNotify(newValue);
            }
        }
    }
}