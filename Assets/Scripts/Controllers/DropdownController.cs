﻿using System.Collections.Generic;
using TMPro;

namespace Controllers
{
    public class DropdownController
    {
        public string CurrentValueText => options[CurrentId].text;
        public int CurrentId { get; private set; }
        
        private readonly List<TMP_Dropdown> dropdowns = new List<TMP_Dropdown>(1);
        private readonly List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();

        public void AddDropdown(TMP_Dropdown dropdown)
        {
            if (dropdowns.Contains(dropdown)) return;
            
            dropdowns.Add(dropdown);
            UpdateDropdown(dropdown);
            dropdown.onValueChanged.AddListener(DropdownChangeValueHandler);
        }

        public void SetOptions(string[] newOptions)
        {
            options.Clear();
            for (int i = 0; i < newOptions.Length; i++)
            {
                var newOption = newOptions[i];
                var newData = new TMP_Dropdown.OptionData(newOption);
                options.Add(newData);
            }
            
            UpdateAllDropdowns();
        }

        public void SetValue(int value)
        {
            DropdownChangeValueHandler(value);
        }

        private void DropdownChangeValueHandler(int newValue)
        {
            CurrentId = newValue;
            UpdateAllDropdowns();
        }

        private void UpdateAllDropdowns()
        {
            for (int i = 0; i < dropdowns.Count; i++)
            {
                var dropdown = dropdowns[i];
                if (dropdown == null)
                {
                    dropdowns.RemoveAt(i);
                    i--;
                    continue;
                }
                
                UpdateDropdown(dropdown);
            }
        }

        private void UpdateDropdown(TMP_Dropdown dropdown)
        {
            dropdown.ClearOptions();
            dropdown.AddOptions(options);
            dropdown.SetValueWithoutNotify(CurrentId);
            dropdown.RefreshShownValue();
        }
    }
}