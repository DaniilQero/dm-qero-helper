﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Controllers
{
    public class ButtonController
    {
        public ButtonController()
        {
            
        }

        public ButtonController(Action onClickAction)
        {
            OnButtonClick += onClickAction;
        }

        public event Action OnButtonClick;
        public event Action<int> OnButtonWithIdClick;
        
        private readonly List<Button> buttons = new List<Button>(1);

        public void AddButton(Button button)
        {
            if (buttons.Contains(button)) return;
            
            buttons.Add(button);
            button.onClick.AddListener(delegate { OnButtonClick?.Invoke(); });
        }

        public void AddButton(Button button, int id)
        {
            if (buttons.Contains(button)) return;
            
            buttons.Add(button);
            button.onClick.AddListener(delegate { OnButtonWithIdClick?.Invoke(id); });
        }
    }
}