using System;
using System.Collections.Generic;
using System.IO;
using Models;
using Models.Database;
using Services;
using UnityEngine;

public static class Core
{
    public enum GameType
    {
        DnD5e, VTM20
    }
    
    public static event Action<Model> OnCurrentModelChange;
    
    public static Model CurrentModel { get; private set; }
    public static string PersistentPatch { get; private set; }
    public static string GamesPatch { get; private set; }
    public static string CurrentGamePatch { get; private set; }

    private static Dictionary<Type, Model> models;

    private const string GamesFolder = "Games";

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void BaseInit()
    {
        PersistentPatch = Application.persistentDataPath;
        GamesPatch = Path.Combine(PersistentPatch, GamesFolder);
        
        LocalizationManager.Init();
        
        models = new Dictionary<Type, Model>
        {
            {typeof(LoadingModel), new LoadingModel()},
            {typeof(EnterToGameModel), new EnterToGameModel()},
            {typeof(MapModel), new MapModel()},
            {typeof(DatabaseModel), new DatabaseModel()},
            {typeof(TimeTrackerModel), new TimeTrackerModel()},
            {typeof(SettingsModel), new SettingsModel()}
        };

        foreach (var pair in models)
        {
            pair.Value.Init();
        }
        
        GetModel<EnterToGameModel>().PrepareFolders();
        GoToModel<EnterToGameModel>();
    }

    public static void AdvancedInit(GameType gameType)
    {
        switch (gameType)
        {
            case GameType.DnD5e:
                models.Add(typeof(CombatModel), new Dnd5eCombatModel());
                break;
            case GameType.VTM20:
                models.Add(typeof(CombatModel), new Vtm20CombatModel());
                break;
            default:
                models.Add(typeof(CombatModel), new CombatModel());
                break;
        }
    }

    public static void GoToModel<T>() where T : Model
    {
        var newModel = GetModel<T>();
        if (newModel != CurrentModel)
        {
            CurrentModel = newModel;
            OnCurrentModelChange?.Invoke(newModel);
        }
    }

    public static void SetCurrentGameName(string name)
    {
        CurrentGamePatch = Path.Combine(GamesPatch, name);
        GetModel<DatabaseModel>().PrepareDatabase();
    }

    private static T GetModel<T>() where T : Model
    {
        return models[typeof(T)] as T;
    }
}