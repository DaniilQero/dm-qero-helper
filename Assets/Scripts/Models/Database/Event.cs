﻿using System.Collections.Generic;

namespace Models.Database
{
    public class Event : IDatabaseItem
    {
        public string Name { get; }
        public List<IDatabaseItem> ChildrenItems { get; }
        public List<string> Tags { get; }
    }
}