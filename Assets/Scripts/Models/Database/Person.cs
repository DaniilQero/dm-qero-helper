﻿using System.Collections.Generic;

namespace Models.Database
{
    public class Person : IDatabaseItem
    {
        public string Name { get; }
        public List<IDatabaseItem> ChildrenItems { get; }
        public List<string> Tags { get; }
    }
}