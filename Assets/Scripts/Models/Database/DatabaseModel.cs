﻿using System;
using System.IO;
using Controllers;
using DefaultNamespace;
using Services;
using UnityEngine;
using Viewers.Properties;

namespace Models.Database
{
    public class DatabaseModel : Model
    {
        public static readonly IntProperty CurrentTab = new();
        public static readonly StringArrayProperty TabsNames = new();
        public static readonly InputFieldController TabNameInputField = new();
        public static readonly ButtonController TabsButtonsController = new();

        private DatabaseSetup databaseSetup;

        private const string DatabaseSetupFileName = "DatabaseSetup";
        private const int TabsCount = 5;
        private string databaseSetupSavePatch;

        public override void Init()
        {
            TabsButtonsController.OnButtonWithIdClick += TabButtonHandler;
        }

        public void PrepareDatabase()
        {
            var gamePatch = Core.CurrentGamePatch;
            databaseSetupSavePatch = Path.Combine(gamePatch, DatabaseSetupFileName);

            try
            {
                var dataText = File.ReadAllText(databaseSetupSavePatch);
                databaseSetup = JsonUtility.FromJson<DatabaseSetup>(dataText);
            }
            catch (Exception _)
            {
                var defaultTabName = LocalizationKeys.DefaultTabName.Localize();
                databaseSetup.tabsNames = new string[TabsCount];
                for (int i = 0; i < TabsCount; i++)
                {
                    databaseSetup.tabsNames[i] = string.Format(defaultTabName, i + 1);
                }
                
                Save();
            }
            
            ChangeTab(databaseSetup.currentTab);
        }

        private void TabButtonHandler(int id)
        {
            ChangeTab(id);
        }

        private void ChangeTab(int newTab)
        {
            CurrentTab.SetValue(newTab);
            TabsNames.SetValue(databaseSetup.tabsNames);
            TabNameInputField.SetText(databaseSetup.tabsNames[newTab]);
        }

        private void Save()
        {
            var setupDataText = JsonUtility.ToJson(databaseSetup, true);
            File.WriteAllText(databaseSetupSavePatch, setupDataText);
        }
    }
}