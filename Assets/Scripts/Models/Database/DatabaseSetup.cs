﻿namespace Models.Database
{
    public struct DatabaseSetup
    {
        public string[] tabsNames;
        public int currentTab;
    }
}