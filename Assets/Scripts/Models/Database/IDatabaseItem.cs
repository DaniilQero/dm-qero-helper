﻿using System.Collections.Generic;

namespace Models.Database
{
    public interface IDatabaseItem
    {
        string Name { get; }
        List<IDatabaseItem> ChildrenItems { get; }
        List<string> Tags { get; }
    }
}