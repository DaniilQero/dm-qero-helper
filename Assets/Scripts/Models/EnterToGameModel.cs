﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Controllers;
using DefaultNamespace;
using Services;
using UnityEngine;
using Viewers.Properties;

namespace Models
{
    public class EnterToGameModel : Model
    {
        public static readonly StringProperty DataPatchText = new();
        public static readonly DropdownController GameSelectDropdown = new();
        public static readonly DropdownController GameTypesDropdown = new();
        public static readonly ButtonController StartGameButton = new();
        public static readonly ButtonController ExitButton = new();
        public static readonly ButtonController RenameGameOkButton = new();
        public static readonly ButtonController CreateNewGameButton = new();
        public static readonly InputFieldController RenameGameInputField = new();
        public static readonly InputFieldController NewGameNameInputField = new();
        public static readonly ButtonController OpenDataDirectoryButton = new();

        public static List<string> Games { get; private set; }
        private static List<GameInfo> gameInfos;

        private const string GameInfoFileName = "GameInfo";

        public override void Init()
        {
            LocalizationManager.OnLanguageChange += UpdateLocalization;
            UpdateLocalization();
            GameTypesDropdown.SetOptions(Enum.GetNames(typeof(Core.GameType)));
        }

        public void PrepareFolders()
        {
            var gamesPatch = Core.GamesPatch;
            if (!Directory.Exists(gamesPatch)) Directory.CreateDirectory(gamesPatch);
            Games = Directory.GetDirectories(gamesPatch).ToList();

            if (Games.Count == 0)
            {
                CreateNewGame(LocalizationKeys.FirstGame.Localize());
            }
            else
            {
                for (var i = 0; i < Games.Count; i++)
                {
                    Games[i] = Path.GetFileName(Games[i]);
                }
                
                gameInfos = new List<GameInfo>(Games.Count);
                for (var i = 0; i < Games.Count; i++)
                {
                    var game = Games[i];
                    var gameInfoFilePatch = $"{gamesPatch}/{game}/{GameInfoFileName}";
                    var gameInfoJson = File.ReadAllText(gameInfoFilePatch);
                    var gameInfo = JsonUtility.FromJson<GameInfo>(gameInfoJson);
                    gameInfos.Add(gameInfo);
                }
            }

            UpdateGamesDropdown();
            Setup();
        }
        
        private void UpdateLocalization()
        {
            DataPatchText.SetValue(string.Format(LocalizationKeys.HelperDataPatch.Localize(), Core.PersistentPatch));
        }

        private void Setup()
        {
            GameSelectDropdown.SetValue(0);

            OpenDataDirectoryButton.OnButtonClick += delegate
            {
                var p = new System.Diagnostics.Process();
                p.StartInfo = new System.Diagnostics.ProcessStartInfo("explorer.exe");
                var patch = Application.persistentDataPath;
                patch = patch.Replace(@"/", @"\");
                p.StartInfo.Arguments = patch;
                p.Start();
            };
            
            RenameGameOkButton.OnButtonClick += delegate
            {
                RenameGameFolder(Games[GameSelectDropdown.CurrentId], RenameGameInputField.CurrentValue);
                Games[GameSelectDropdown.CurrentId] = RenameGameInputField.CurrentValue;
                UpdateGamesDropdown();
            };
            
            CreateNewGameButton.OnButtonClick += delegate
            {
                CreateNewGame(NewGameNameInputField.CurrentValue);
                UpdateGamesDropdown();
                GameSelectDropdown.SetValue(Games.Count - 1);
            };

            StartGameButton.OnButtonClick += delegate
            {
                Core.SetCurrentGameName(Games[GameSelectDropdown.CurrentId]);
                Core.AdvancedInit(gameInfos[GameSelectDropdown.CurrentId].gameType);
                Core.GoToModel<MapModel>();
            };
            
#if UNITY_EDITOR
            ExitButton.OnButtonClick += delegate { UnityEditor.EditorApplication.isPlaying = false; };
#else
            ExitButton.onButtonClick += Application.Quit;
#endif
        }
        
        private void RenameGameFolder(string oldName, string newName)
        {
            var oldPatch = $"{Core.GamesPatch}/{oldName}";
            var newPatch = $"{Core.GamesPatch}/{newName}";

            Directory.Move(oldPatch, newPatch);
        }

        private void CreateNewGame(string newGameName)
        {
            var gamesPatch = Core.GamesPatch;
            var newGamePatch = $"{gamesPatch}/{newGameName}";
            Directory.CreateDirectory(newGamePatch);

            var newGameType = (Core.GameType)GameTypesDropdown.CurrentId;
            var newGameInfo = new GameInfo
            {
                gameType = newGameType
            };

            var infoJson = JsonUtility.ToJson(newGameInfo, true);
            File.WriteAllText($"{newGamePatch}/{GameInfoFileName}", infoJson);
            
            Games ??= new List<string>();
            gameInfos ??= new List<GameInfo>();
            
            Games.Add(newGameName);
            gameInfos.Add(newGameInfo);
        }
        
        private void UpdateGamesDropdown()
        {
            var lastSelected = GameSelectDropdown.CurrentId;
            var dropdownGames = new string[Games.Count];

            for (int i = 0; i < Games.Count; i++)
            {
                dropdownGames[i] = $"{Games[i]} ({gameInfos[i].gameType.ToString()})";
            }

            GameSelectDropdown.SetOptions(dropdownGames);
            GameSelectDropdown.SetValue(lastSelected);
        }
        
        [Serializable]
        private struct GameInfo
        {
            public Core.GameType gameType;
        }
    }
}