﻿using Controllers;
using Models.Database;

namespace Models
{
    public abstract class Model
    {
        public abstract void Init();
        
        public static readonly ButtonController MapButtonController = new ButtonController(delegate
        {
            Core.GoToModel<MapModel>();
        });
        
        public static readonly ButtonController DatabaseButtonController = new ButtonController(delegate
        {
            Core.GoToModel<DatabaseModel>();
        });
        
        public static readonly ButtonController TimeTrackerButtonController = new ButtonController(delegate
        {
            Core.GoToModel<TimeTrackerModel>();
        });
        
        public static readonly ButtonController CombatButtonController = new ButtonController(delegate
        {
            Core.GoToModel<CombatModel>();
        });
        
        public static readonly ButtonController SettingsButtonController = new ButtonController(delegate
        {
            Core.GoToModel<SettingsModel>();
        });
    }
}