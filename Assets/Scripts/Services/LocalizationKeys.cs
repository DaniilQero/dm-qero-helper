﻿namespace Services
{
    public static class LocalizationKeys
    {
        public const string HelperDataPatch = "HelperDataPatch";
        public const string FirstGame = "FirstGame";
        public const string DefaultTabName = "DefaultTabName";
    }
}