﻿using System;
using UnityEngine;

namespace Services
{
    public static class LocalizationManager
    {
        public static event Action OnLanguageChange;
        public static SystemLanguage systemLanguage => realization.CurrentLanguage;

        private static ILocalizationServiceRealization realization;

        public static void Init()
        {
            realization = new LocalizationDefaultRealization();
            SetLanguage(Application.systemLanguage);
        }

        public static string GetText(string key) => realization.GetText(key);

        public static void SetLanguage(SystemLanguage newLanguage)
        {
            realization.SetLanguage(newLanguage);
            OnLanguageChange?.Invoke();
        }
    }

    public interface ILocalizationServiceRealization
    {
        string GetText(string key);
        void SetLanguage(SystemLanguage newLanguage);
        SystemLanguage CurrentLanguage { get; }
    }
}