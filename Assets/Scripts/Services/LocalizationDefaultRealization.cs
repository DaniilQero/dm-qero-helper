﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

namespace Services
{
    public class LocalizationDefaultRealization : ILocalizationServiceRealization
    {
        private const string LocalizationFileName = "LocalizationFile.xml";

        private Dictionary<string, Dictionary<string, string>> data;
        public SystemLanguage CurrentLanguage { get; private set; }

        public LocalizationDefaultRealization()
        {
            var patch = $"{Application.streamingAssetsPath}/{LocalizationFileName}";

            if (!Directory.Exists(Application.streamingAssetsPath))
            {
                Directory.CreateDirectory(Application.streamingAssetsPath);
            }
                
            if (!File.Exists(patch))
            {
                data = new Dictionary<string, Dictionary<string, string>>();

                var eng = SystemLanguage.English.ToString();
                data.Add(eng, new Dictionary<string, string>());
                data[eng].Add("key1", "Test key 1");
                data[eng].Add("key2", "Test key 2");

                new XElement("Languages", data.Select(kv => new XElement(kv.Key, kv.Value.Select(kv2 => new XElement(kv2.Key, kv2.Value))))).Save(patch, SaveOptions.OmitDuplicateNamespaces);
            }
            else
            {
                data = XElement.Parse(File.ReadAllText(patch)).Elements().ToDictionary(k => k.Name.ToString(), v => XElement.Parse(v.ToString()).Elements().ToDictionary(k2 => k2.Name.ToString(), v2 => v2.Value.ToString()));
            }

            SetLanguage(SystemLanguage.English);
        }

        public string GetText(string key)
        {
            if (!data.TryGetValue(CurrentLanguage.ToString(), out var keys))
            {
                if (!data.TryGetValue(SystemLanguage.English.ToString(), out var engKeys))
                {
                    engKeys = new Dictionary<string, string>();
                    data[SystemLanguage.English.ToString()] = engKeys;
                }
                
                keys = data[SystemLanguage.English.ToString()];
            }

            if (!keys.TryGetValue(key, out var text))
            {
                text = $"[!missing {key}!]";
            }

            return text;
        }

        public void SetLanguage(SystemLanguage newLanguage)
        {
            CurrentLanguage = newLanguage;
        }
    }
}